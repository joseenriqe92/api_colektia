const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController')

router.post('/register', async (req, res, next) => {
  await  userController.registerUser(req, res)
  });

router.get('/alluser', async (req, res, next) => {
  await userController.getAllUser(req, res)
});

router.put('/updateuser', async (req, res) => {
  await  userController.updateUser(req, res);
});

router.delete('/delete/:id', async (req, res) => {
  await  userController.deleteUser(req, res);
});

module.exports = router;

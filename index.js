const path = require('path');
const express = require('./config/express')

const app = express();

const { mongoose } = require('./config/database');

// Settigns
app.set('port', process.env.PORT || 8081);

//Starting
app.listen(app.get('port'), () => {
	console.log('Server running...');
});

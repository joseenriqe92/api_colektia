require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const timeout = require('connect-timeout');
const routes = require('../routes')

const App = () => {

  const app = express();

  app.use(morgan('dev'));
  app.use(cors());
  //app.use(express.urlencoded({extend: false})); //recibo imagenes desde form
  app.use(express.json());

  app.use((req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
  });

  app.use(timeout(120000));

  app.use(haltOnTimedout);

  function haltOnTimedout(req, res, next) {
      if (!req.timedout) next();
  }

  //Routes
try {
  routes(app)
} catch (e) {
  console.log(e);  
}

  return app;
};
module.exports = App

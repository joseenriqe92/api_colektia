const pg = require('pg');

// ENVIRONMENT VARIABLES
const postgres = {
  host: process.env.HOST,
  port: 5432,
  database: process.env.DATABASE,
  user: process.env.USERDB,
  password: process.env.PASS,
  ssl: true
};
const connectionString = postgres;
const pool = new pg.Pool(connectionString);

module.exports = pool;

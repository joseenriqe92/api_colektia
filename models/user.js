const pool = require('../config/database');

const insertUser = (name, phone) => { 
	const pgQuery = (`INSERT INTO public."user"(
	  name, phone, available)
  VALUES ('${name}', '${phone}', TRUE );`);
	return new Promise((resolve, reject) => {
	  pool.connect((err, client, done) => {
		client.query(pgQuery, (err, res) => {
		  done()
		  if (err) {
			console.log(err.stack);
		  } else {
			resolve(res.rows[0]);
		  }
		})
	  })
	});
  }

const getAllUser = () => { 
	const pgQuery = (`SELECT id_user, name, phone FROM "user"
	WHERE available = TRUE`);
	return new Promise((resolve, reject) => {
	  pool.connect((err, client, done) => {
		client.query(pgQuery, (err, res) => {
		  done()
		  if (err) {
			console.log(err.stack);
		  } else {
			resolve(res.rows);
		  }
		})
	  })
	});
  }

const updateUser = (idUser, name, phone) => { 
	const pgQuery = (`UPDATE public."user"
	SET  "name"='${name}', phone='${phone}'
	WHERE id_user = ${idUser}`);
	return new Promise((resolve, reject) => {
	  pool.connect((err, client, done) => {
		client.query(pgQuery, (err, res) => {
		  done()
		  if (err) {
			console.log(err.stack);
		  } else {
			resolve(res.rows[0]);
		  }
		})
	  })
	});
  };

const deleteUser = (idUser) => { 
	const pgQuery = (`UPDATE public."user"
	SET  "available"= FALSE
	WHERE id_user = ${idUser}`);
	return new Promise((resolve, reject) => {
	  pool.connect((err, client, done) => {
		client.query(pgQuery, (err, res) => {
		  done()
		  if (err) {
			console.log(err.stack);
		  } else {
			resolve(res.rows);
		  }
		})
	  })
	});
}
module.exports = {
	insertUser,
	getAllUser,
	updateUser,
	deleteUser	
}
  
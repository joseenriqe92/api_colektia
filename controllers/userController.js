const user = require('../models/user')

const registerUser = async (req, res) => {
  try {
    const {name, phone} = req.body;
    await user.insertUser(name, phone);
    const result  ={
      status: 200,
      success: true,
      message : 'Contacto insertado con exito.'
    }
    res.send(result);
  } catch (e) {
    const result  ={
      status: 400,
      success: false,
      message : 'Ha existido un error, intente mas tarde.'
    }
    res.send(result);
    console.log(e);
  }
}

const getAllUser = async (req, res) => {
  try {
    const pgQuery = await user.getAllUser();
    const result  ={
      status: 200,
      success: true,
      data : pgQuery
    }
    res.send(result);
  } catch (e) {
    const result  ={
      status: 400,
      success: false,
      message : 'Ha existido un error, intente mas tarde.'
    }
    res.send(result);
    console.log(e);
  }
}

const updateUser = async (req, res) => {
  try {
    const {idUser, name, phone} = req.body
    await user.updateUser(idUser, name, phone);
    const result  ={
      status: 200,
      success: true,
      message : 'Contacto actualizado con exito.',
    }
    res.send(result);
  } catch (e) {
    const result  ={
      status: 400,
      success: false,
      message : 'Ha existido un error, intente mas tarde.'
    }
    res.send(result);
    console.log(e);
  }
} 

const deleteUser = async (req, res) => {
  try {
    const idUser = req.params.id;
    await user.deleteUser(idUser);
    const result  ={
      status: 200,
      success: true,
      message : 'Contacto eliminado con exito.',
    }
    res.send(result);
  } catch (e) {
    const result = {
      status: 400,
      success: false,
      message : 'Ha existido un error, intente mas tarde.'
    }
    res.send(result);
    console.log(e);
  }
} 

// insertUser();
module.exports = {
  registerUser,
  getAllUser,
  updateUser,
  deleteUser
}
